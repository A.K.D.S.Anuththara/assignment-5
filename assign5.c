#include<stdio.h>

int NumAttendees(int price);
int Income(int price);
int Expenditure(int price);
int Profit(int price);


int	NumAttendees(int price){
	return 120-(price-15)/5*20;
}

int Income(int price){
	return NumAttendees(price)*price;
}

int Expenditure(int price){
	return NumAttendees(price)*3+500;
}

int Profit(int price){
	return Income(price)-Expenditure(price);
}

int main(){
	int price;
	printf("You can find the highest profit: \n");
	for(price=5;price<50;price+=5)
	{
		printf("Ticket Price = Rs.%d \t Profit = Rs.%d\n",price,Profit(price));

    }
		return 0;
	}
